<?php


/**
 * @SuppressWarnings(PHPMD)
*/
class ApiTester extends \Codeception\Actor
{
    use _generated\ApiTesterActions;

   /**
    * Verifica se a resposta é code 201 = CREATED e um json informando o ID
    */
    public function AssertNoError(ApiTester $I){
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
        $I->seeResponseIsJson();
        $I->dontSeeResponseContains(\Codeception\Util\HttpCode::UNPROCESSABLE_ENTITY);
    }

    /**
     * Verifica se a resposta é um status code 422 = UNPROCESSABLE_ENTITY com a mensagem de erro.
     * Significando que o cadastro falhou pelo fato do cpf, email ou telefone já ter sido cadastrado ou qualquer outro problema de validação.
     */
    public function AssertError(ApiTester $I){
        $I->dontSeeResponseCodeIs(\Codeception\Util\HttpCode::CREATED);
        $I->seeResponseCodeIs(\Codeception\Util\HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseContains('$example[\'document\']');
    }
}
