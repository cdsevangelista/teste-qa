<?php
use Codeception\Example;

class VerifyFormCest
{
    const CREATE_USER_ENDPOINT = "/register";

    protected $bearerToken;
    protected $apiSettings;
    protected $registerRequest;

    /**
     * Basic Token é necessário para funcionamento do teste
    */
    public function _before(ApiTester $I)
    {
        $config = \Codeception\Configuration::config();
        $this->apiSettings = \Codeception\Configuration::suiteSettings('api', $config);

        $this->bearerToken = $this->apiSettings["tokens"]["bearerToken"];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Authorization', $this->bearerToken);

        $this->registerRequest = [
            "firstname"=> "Caroline",
            "lastname"=> "Santos",
            "document"=> "99988877732",
            "birthdate"=> "1945-01-05",
            "email"=> "email@domain.com",
            "password"=> "password",
            "phone_mobile"=> "5398783214",
            "phone_fixed"=> "5333333333",
            "company"=> "Loja do kpop",

            "coupon"=> "MELHORLOJA",

            "terms"=> 1,

            "adress"=> [
                "postal_code" => "96020000",
                "label"=> "Meu Endereco",
                "address"=> "Rua General Osório",
                "number"=> "659",
                "complement"=> "",
                "district"=> "Centro",
                "city"=> "Pelotas",
                "state_abbr"=> "RS",
                "country"=> "BR"
            ]
        ];
    }

    public function SuccessfulRegister(ApiTester $I)
    {
        $I->wantTo("Verifica se o cadastro será efeuado normalmente");

        $I->sendPOST(SELF::CREATE_USER_ENDPOINT, $this->registerRequest);

        $I->AssertNoError($I);

        $I->seeResponseMatchesJsonType([
            'id' => 'string'
        ]);
    }

    /**
     * Algumas informações são substituidas por respostas que invalidam o cadastro
     *
     * @dataProvider invalidValuesProvider
     */
    public function FailRegister(ApiTester $I, Codeception\Example $example){
        $I->wantTo("Verifica que o cadastro não sera realizado por problemas de validação");

        $this->registerRequest['document'] = $example['document'];
        $this->registerRequest['email'] = $example['email'];
        $this->registerRequest['terms'] = $example['terms'];
        $this->registerRequest['postal_code'] = $example['postal_code'];

        $I->sendPOST(SELF::CREATE_USER_ENDPOINT, $this->registerRequest);

        $I->AssertError($I);
    }

    protected function invalidValuesProvider(){
        return [
            ['document' => null, 'email'=> 'email@domain.com','terms' => 0,'postal_code' => 1]
        ];
    }
}
