<?php

use Codeception\Example;

class VerifyFormTest extends \Codeception\Test\Unit
{
    /**
     * @var UnitTester
     */
    protected $registerRequest;

    const TERMS_DENIED = 0;
    const ADRESS_REQUIRED_FIELD = ['postal_code', 'address', 'number', 'city', 'state_abbr'];

    protected function _before()
    {
        $this->registerRequest = [
            "firstname"=> "Caroline",
            "lastname"=> "Santos",
            "document"=> "99988877732",
            "birthdate"=> "1945-01-05",
            "email"=> "email@domain.com",
            "password"=> "password",
            "phone_mobile"=> "5398783214",
            "phone_fixed"=> "5333333333",
            "company"=> "Loja do kpop",

            "coupon"=> "MELHORLOJA",

            "terms"=> 1,

            "adress"=> [
                "label"=> "Meu Endereco",
                "postal_code" => "96020000",
                "address"=> "Rua General Osório",
                "number"=> "659",
                "complement"=> "",
                "district"=> "Centro",
                "city"=> "Pelotas",
                "state_abbr"=> "RS",
                "country"=> "BR"
            ]
        ];
    }

    /**
     * Verifica se todos os campos obrirgatórios não são nulos,
     * se email é um tipo de email valido e se os termos de uso foram
     * preenchidos
    */
    public function testSuccessfulRegister()
    {
        $this->assertTrue(is_string($this->registerRequest['firstname']) && !is_null($this->registerRequest['firstname']));
        $this->assertTrue(is_string($this->registerRequest['lastname']) && !is_null($this->registerRequest['lastname']));
        $this->assertTrue(is_string($this->registerRequest['document']) && !is_null($this->registerRequest['document']));
        $this->assertTrue(is_string($this->registerRequest['birthdate']) && !is_null($this->registerRequest['birthdate']));
        $this->assertIsString(filter_var($this->registerRequest['email'], FILTER_VALIDATE_EMAIL));
        $this->assertTrue(!is_null($this->registerRequest['password']));
        $this->assertTrue(is_string($this->registerRequest['phone_mobile']) && !is_null($this->registerRequest['phone_mobile']));
        $this->assertEmpty(!$this->registerRequest['terms']);

        foreach (SELF::ADRESS_REQUIRED_FIELD as $required){
            $this->assertNotEmpty($this->registerRequest['adress'][$required]);
        }
    }

    /**
     * Teste espera a falha no cadastro
     * Verifica se o nome não é uma String, se é nulo e se os termos
     * de uso não foram aceitos
     */
    public function testFailRegister()
    {
        $this->registerRequest['firstname'] = null;
        $this->registerRequest['terms'] = SELF::TERMS_DENIED;

        $this->assertTrue(is_string($this->registerRequest['firstname']) && !is_null($this->registerRequest['firstname']));
        $this->assertTrue(is_string($this->registerRequest['lastname']) && !is_null($this->registerRequest['lastname']));
        $this->assertTrue(is_string($this->registerRequest['document']) && !is_null($this->registerRequest['document']));
        $this->assertTrue(is_string($this->registerRequest['birthdate']) && !is_null($this->registerRequest['birthdate']));
        $this->assertIsString(filter_var($this->registerRequest['email'], FILTER_VALIDATE_EMAIL));
        $this->assertTrue(!is_null($this->registerRequest['password']));
        $this->assertTrue(is_string($this->registerRequest['phone_mobile']) && !is_null($this->registerRequest['phone_mobile']));
        $this->assertEmpty(!$this->registerRequest['terms']);

        foreach (SELF::ADRESS_REQUIRED_FIELD as $required){
            $this->assertNotEmpty($this->registerRequest['adress'][$required]);
        }
    }
}