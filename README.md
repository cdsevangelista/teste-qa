Este repositório consiste em um projeto criado a partir do framework Codeception e contém dois tipos de testes (API e unitário) para a etapa de cadastro da plataforma do ME.

**Objetivos :**


Aplicar um teste automatizado, com a ferramenta / framework que achar mais conveniente para testar a etapa de cadastro em nossa plataforma. Considerando ao menos 1 cenário positivo, onde o mesmo é cadastrado e 1 cenário negativo, onde o mesmo deve falhar por alguma validação por exemplo.


**Como rodar:**


git clone git@bitbucket.org:cdsevangelista/teste-qa.git 

Vai clonar o repositório para a pasta

Rode esse comando no terminal (pode demorar uns minutos):

`composer require "codeception/codeception" --dev
`




**Para executar os testes:**


Unitário:

`./vendor/bin/codecept run unit VerifyFormTest 
`

API:

`./vendor/bin/codecept run api VerifyFormCest 
`

_**Mais informações sobre funcionamento na Documentação.**_


https://bitbucket.org/cdsevangelista/teste-qa/wiki/Home


Have fun!